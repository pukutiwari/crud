// import React from 'react';
// import "antd/dist/antd.css";

// import { Button, Table, Modal, Input } from "antd";
// import { useState,useEffect } from "react";
// import { EditOutlined, DeleteOutlined } from "@ant-design/icons";


// const EditUser=( {onAdd,onOk,onEdit} )=>{

//     const [isModalOpen, setIsModalOpen] = useState(false);

//   const showModal = () => {
//     setIsModalOpen(true);
//   };

//   const handleOk = () => {
//     setIsModalOpen(false);
//   };

//   const handleCancel = () => {
//     setIsModalOpen(false);
//   };
//   const [state,setState]=useState("");

//     const handleOnSubmit=(e)=>{
//         e.preventDefault();
//     //         let form_data = new FormData();
//     // form_data.append('image', state.image);
//     // onEdit(e.target.name.value,e.target.email.value,e.target.image.value);
//         onOk();
//         onAdd(e.target.name.value,e.target.email.value);
        
//         e.target.name.value="";
//         e.target.email.value="";
//         // e.target.image.value="";
//     }

   
//     // const [selectedImage, setSelectedImage] = useState(null);


//     return (
//         <div>
//             <form onSubmit={handleOnSubmit}>
          
//             <h3>Add User</h3> 
//             <div>
    
//       <br />
     
//       <br /> 

//     </div>

//             {/* <input type="file"  name="image"/> */}

                    
      
         
            
//             <input placeholder="Name"  name="name"/><br/><br/>
//             <input placeholder="Email"  name="email"/>
//             <button onSubmit={handleOnSubmit} >Add</button>


//             </form>
           

//         </div>
//     )
// }

// export default EditUser;

// import { Typography, Box, makeStyles, Grid, TextField, Button } from "@material-ui/core"
// import { deepPurple, green } from '@material-ui/core/colors';
import { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";


const Edit = () => {
//  const classes = useStyles();
 const { id } = useParams();
//  const history = useHistory();
 const [student, setStudent] = useState({
  name: "",
  email: ""
 });
 useEffect(() => {
  async function getStudent() {
   try {
    const student = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
    // console.log(student.data);
    setStudent(student.data);
   } catch (error) {
    console.log("Something is Wrong");
   }
  }
  getStudent();
 }, [id]);

 function onTextFieldChange(e) {
  setStudent({
   ...student,
   [e.target.name]: e.target.value
  })
 }

 async function onFormSubmit(e) {
  e.preventDefault()
  try {
   await axios.put(`https://jsonplaceholder.typicode.com/users/${id}`, student)
   history.push("/")
  } catch (error) {
   console.log("Something is Wrong");
  }
 }
 function handleClick() {
  history.push("/")
 }
 return (
  <>


   {/* <Grid container justify="center" spacing={4}> */}
    {/* <Grid item md={6} xs={12}> */}
 
     <form>
 
        <input autoComplete="id" name="id" variant="outlined" required fullWidth id="id" label="ID" autoFocus value={id} disabled />
     
        <input autoComplete="stuname" name="stuname" variant="outlined" required fullWidth id="stuname" label="Name" value={student.stuname} onChange={e => onTextFieldChange(e)} />
     
        <input autoComplete="email" name="email" variant="outlined" required fullWidth id="email" label="Email Address" value={student.email} onChange={e => onTextFieldChange(e)} />
       
       <button type="button" variant="contained" color="primary" fullWidth onClick={e => onFormSubmit(e)}> Update </button>
 
     </form>
  
      <button variant="contained" color="primary" onClick={handleClick}>Back to Home</button>
    
 
  </>
 )
}

export default Edit